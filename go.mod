module gitlab.com/jaxnet/common/netstack

go 1.15

require (
	gitlab.com/jaxnet/common/marshaller v0.0.0-20210111115116-dfe56965e53f
	github.com/rs/zerolog v1.20.0
	github.com/google/uuid v1.2.0
	gitlab.com/jaxnet/core/shard.core v0.1.6
)
