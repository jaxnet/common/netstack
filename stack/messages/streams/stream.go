package streams

import (
	"gitlab.com/jaxnet/common/netstack/stack/messages/common"
	"net"
)

type MessagesStream struct {
	conn net.Conn
}

func NewMessagesStream(conn net.Conn) (stream *MessagesStream) {
	stream = &MessagesStream{conn: conn}
	return
}

func (stream *MessagesStream) NextMessage() (msg common.Message, err error) {
	return nil, nil
}
