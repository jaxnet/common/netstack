package communicators

import (
	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/common/netstack/stack/messages/common"
	"gitlab.com/jaxnet/common/netstack/stack/proto"
)

type Communicator interface {
	Follow(resolvingParams string, handlers []proto.StateHandler) error
	Serve(resolvingParams string, handlers []proto.StateHandler) error
	SendMessage(msg common.Message) error

	Logger() *zerolog.Logger
}
