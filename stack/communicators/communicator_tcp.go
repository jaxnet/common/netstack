package communicators

import (
	"fmt"
	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/common/netstack/implementation/streams"
	"gitlab.com/jaxnet/common/netstack/stack/ec"
	"gitlab.com/jaxnet/common/netstack/stack/messages/common"
	"gitlab.com/jaxnet/common/netstack/stack/proto"
	"net"
)

type TCPCommunicator struct {
	protocol proto.Protocol
	logger   *zerolog.Logger
}

func NewTCPCommunicator(proto proto.Protocol, logger *zerolog.Logger) (comm *TCPCommunicator) {
	comm = &TCPCommunicator{
		protocol: proto,
		logger:   logger,
	}

	return
}

func (communicator *TCPCommunicator) processStates(conn net.Conn, client bool) (err error) {
	var states []*proto.State
	if client {
		states = communicator.protocol.ClientStates()
	} else {
		states = communicator.protocol.ServerStates()
	}

	if len(states) == 0 {
		msg := "states set must contain at least one state: %w"
		err = fmt.Errorf(msg, ec.ErrInvalidStatesSet)
		return
	}
	state := states[0]

	var (
		ctx            = proto.NewContext()
		messagesStream = streams.NewMessagesStream(conn)
	)

	for {
		if state == proto.FinalState {
			communicator.closeSession(conn)
			return
		}

		// Important!
		// This message structure must be refreshed each time next state is going to be processed.
		// Otherwise, states would receive messages that has been received earlier.
		var incomingMessage common.Message

		// If state expects some message to be delivered - fetch it from the network first.
		// Otherwise - execute the state "as is".
		if state.ExpectsIncomingMessages() {
			incomingMessage, err = messagesStream.ReadNextForState(state)
			if err != nil {
				// WARN:
				// It is important to interrupt connection here!
				//
				// There is the case, when received data flow does not corresponds to any message expected.
				// Once this cae happens, only the message header is read in memory, but the rest of message body
				// (that can be potentially very big) still sits in a socket's memory.
				// Continuing to read this malformed data flow could be dangerous (at least by the memory overflow).
				// But there is no any way to drop only irrelevant data from the socket.
				// So whole connection must be dropped here.
				communicator.closeSession(conn)
				communicator.logger.Err(err).Msg("")
				return
			}
		}

		nextStateCandidate, outgoingMessage, err := state.Handle(ctx, incomingMessage)
		if err != nil {
			communicator.closeSession(conn)
			err = fmt.Errorf("internal state error: %s: %w", err.Error(), ec.ErrInternalStateError)
			return err
		}

		if outgoingMessage != nil {
			err = messagesStream.SendForState(state, outgoingMessage)
			if err != nil {
				communicator.closeSession(conn)
				communicator.logger.Err(err).Msg("")
				return err
			}
		}

		if nextStateCandidate == nil {
			break

		} else {
			// State has been executed correctly.
			// Protocol can be switched to the next stage.
			//
			// Looking for the next state to be present.
			// (runtime check that ensures that handler returned correct next state that corresponds with the proto)
			if isNextStateCandidateValid(state, nextStateCandidate) == false {
				panic("No state candidate found for the returned state.")
			}

			state = nextStateCandidate
		}
	}

	return
}

func (communicator *TCPCommunicator) closeSession(conn net.Conn) {
	// todo: log here

	err := conn.Close()
	if err != nil {
		// todo: log error here
	}
}

func isNextStateCandidateValid(state, candidate *proto.State) (valid bool) {
	for _, stateCandidate := range state.NextState {
		if stateCandidate == candidate {
			valid = true
			return
		}
	}

	return
}
