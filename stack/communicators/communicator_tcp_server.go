package communicators

import (
	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/common/netstack/stack/proto"
	"net"
)

func (communicator *TCPCommunicator) Serve(resolvingParams string, handlers []proto.StateHandler) (err error) {
	err = communicator.protocol.SetServerStateHandlers(handlers)
	if err != nil {
		return
	}

	listener, err := net.Listen("tcp", resolvingParams)
	if err != nil {
		return
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			return err
		}

		communicator.handleNewConnectionAsync(conn)
	}
}

func (communicator *TCPCommunicator) handleNewConnectionAsync(conn net.Conn) {
	go func() {
		//goland:noinspection GoUnhandledErrorResult
		defer conn.Close()

		// Establishing context logging context.
		// Next several methods are used for logging client-related events
		// and add some additional context to each log-record.
		info := func() *zerolog.Event {
			return communicator.logger.Info().Str("client", conn.RemoteAddr().String())
		}

		e := func(err error) *zerolog.Event {
			return communicator.logger.Err(err).Str("client", conn.RemoteAddr().String())
		}

		info().Msg("New P2P connection established")
		err := communicator.protocol.PrepareForServer(conn)
		if err != nil {
			e(err).Msg("Connection preparing failed. Connection closed.")
			return
		}

		err = communicator.processStates(conn, false)
		if err != nil {
			e(err).Msg("Internal states processing error. Client connection closed.")
			return
		}
	}()
}
