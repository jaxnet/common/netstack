package communicators

import (
	"gitlab.com/jaxnet/common/netstack/stack/proto"
	"net"
)

func (communicator *TCPCommunicator) Follow(resolvingParams string, handlers []proto.StateHandler) (err error) {
	err = communicator.protocol.SetClientStateHandlers(handlers)
	if err != nil {
		return
	}

	conn, err := net.Dial("tcp", resolvingParams)
	if err != nil {
		return
	}
	//goland:noinspection GoUnhandledErrorResult
	defer conn.Close()

	err = communicator.protocol.PrepareForClient(conn)
	if err != nil {
		return
	}

	// Note: In contrast with server, client-side connection is not processed in separate goroutine,
	//		 cause it is expected client ot have only one session per time with server.
	//		 In case if more sessions needed - mor clients could be crated by the third-party developer.
	return communicator.processStates(conn, true)
}
