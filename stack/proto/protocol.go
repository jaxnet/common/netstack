package proto

import "net"

type Protocol interface {
	SetClientStateHandlers(handlers []StateHandler) error
	SetServerStateHandlers(handlers []StateHandler) error

	ClientStates() []*State
	ServerStates() []*State

	// Prepare is expected to be called when connection is established,
	// and allows to do some protocol-specific things, like tuning or version check.
	PrepareForClient(conn net.Conn) (err error)
	PrepareForServer(conn net.Conn) (err error)
}
