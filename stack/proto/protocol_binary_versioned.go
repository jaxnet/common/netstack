package proto

import (
	"encoding/binary"
	"fmt"
	"gitlab.com/jaxnet/common/netstack/stack/ec"
	"net"
)

type VersionedBinaryProtocol struct {
	BinaryProtocol
	Version uint16
}

func NewVersionedBinaryProtocol(ver uint16, client, server []*State) (proto *VersionedBinaryProtocol, err error) {
	lowerProtocol, err := NewBinaryProtocol(client, server)
	if err != nil {
		return
	}

	proto = &VersionedBinaryProtocol{
		Version:        ver,
		BinaryProtocol: *lowerProtocol,
	}

	return
}

func (proto VersionedBinaryProtocol) PrepareForClient(conn net.Conn) (err error) {
	versionBinary := []byte{0, 0}
	binary.BigEndian.PutUint16(versionBinary, proto.Version)

	err = proto.sendBytes(conn, versionBinary)
	if err != nil {
		return
	}

	responseCode := []byte{255}
	err = proto.readBytes(conn, responseCode)
	if err != nil {
		return
	}

	if responseCode[0] != 0 {
		err = fmt.Errorf("protocol version has not been accepted by the server: %w", ec.ErrProtocolVersion)
		return
	}

	return
}

func (proto VersionedBinaryProtocol) PrepareForServer(conn net.Conn) (err error) {
	versionBinary := []byte{0, 0}
	err = proto.readBytes(conn, versionBinary)
	if err != nil {
		return
	}

	var (
		versionIsOK         = []byte{0}
		versionIsUnexpected = []byte{1}
	)

	version := binary.BigEndian.Uint16(versionBinary)
	if version != proto.Version {
		_ = proto.sendBytes(conn, versionIsUnexpected)
		err = ec.ErrProtocolVersion
		return

	} else {
		err = proto.sendBytes(conn, versionIsOK)
		return
	}
}

func (proto VersionedBinaryProtocol) ClientStates() []*State {
	return proto.Client
}

func (proto VersionedBinaryProtocol) ServerStates() []*State {
	return proto.Server
}

func (proto VersionedBinaryProtocol) sendBytes(conn net.Conn, data []byte) (err error) {
	bytesWritten, err := conn.Write(data)
	if err != nil {
		return
	}

	if bytesWritten != len(data) {
		err = fmt.Errorf("can't send data: %w", ec.ErrTransportFailed)
	}

	return
}

func (proto VersionedBinaryProtocol) readBytes(conn net.Conn, buffer []byte) (err error) {
	bytesRead, err := conn.Read(buffer)
	if err != nil {
		return
	}

	if bytesRead != len(buffer) {
		err = fmt.Errorf("can't read data: %w", ec.ErrTransportFailed)
	}

	return
}
