package proto

import (
	"gitlab.com/jaxnet/common/netstack/stack/messages/common"
	"reflect"
)

var (

	// FinalState is a syntactic sugar for marking last states in a protocol.
	FinalState *State = nil

	// RecursiveState is used when some state must enter in the same state once again,
	// e.g. in case if some there is some failure in sending/receiving some message
	// and the operation must be done once more from the same state.
	RecursiveState = &State{}

	// NoMoreStates is a syntactic sugar for marking cases when state has no any descendants.
	NoMoreStates = []*State{FinalState}
)

type State struct {
	Handle           StateHandler
	IncomingMessages []common.Message
	OutgoingMessages []common.Message
	NextState        []*State
}

func (state *State) ExpectsIncomingMessages() bool {
	return len(state.IncomingMessages) > 0
}

func (state *State) ExpectsToReceiveMessage(msg common.Message) (expected bool) {
	for _, expectedMessage := range state.IncomingMessages {
		if reflect.TypeOf(expectedMessage) == reflect.TypeOf(msg) {
			expected = true
			return
		}
	}

	return
}

func (state *State) ExpectsToSendMessage(msg common.Message) (expected bool) {
	for _, expectedMessage := range state.OutgoingMessages {
		if reflect.TypeOf(expectedMessage).String() == reflect.TypeOf(msg).String() {
			expected = true
			return
		}
	}

	return
}

type StateHandler func(ctx *Context, msg common.Message) (nextState *State, outgoingMessage common.Message, err error)
