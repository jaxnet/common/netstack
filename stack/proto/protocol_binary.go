package proto

import (
	"fmt"
	"gitlab.com/jaxnet/common/netstack/stack/ec"
)

type BinaryProtocol struct {
	Client []*State
	Server []*State
}

func NewBinaryProtocol(client, server []*State) (proto *BinaryProtocol, err error) {
	if len(client) == 0 && len(server) == 0 {
		err = fmt.Errorf("states set can't be empty: %w", ec.ErrInvalidStatesSet)
		return
	}

	proto = &BinaryProtocol{
		Client: client,
		Server: server,
	}

	return
}

func (proto *BinaryProtocol) SetClientStateHandlers(handlers []StateHandler) (err error) {
	if len(proto.Client) != len(handlers) {
		msg := "handlers set must contain implementation for each interaction: %w"
		err = fmt.Errorf(msg, ec.ErrInvalidStatesSet)
		return
	}

	return proto.setHandlers(handlers, true)
}

func (proto *BinaryProtocol) SetServerStateHandlers(handlers []StateHandler) (err error) {
	if len(proto.Server) != len(handlers) {
		msg := "handlers set must contain implementation for each interaction: %w"
		err = fmt.Errorf(msg, ec.ErrInvalidStatesSet)
		return
	}

	return proto.setHandlers(handlers, false)
}

func (proto *BinaryProtocol) setHandlers(handlers []StateHandler, client bool) (err error) {
	var state *State
	for i, handler := range handlers {

		if client {
			state = proto.Client[i]
		} else {
			state = proto.Server[i]
		}

		if state == nil {
			msg := "non initialized interaction state occurred (can't initialize handler for non-initialized state): %w"
			err = fmt.Errorf(msg, ec.ErrInvalidStatesSet)
			return
		}

		state.Handle = handler
	}

	return
}
