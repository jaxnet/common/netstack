package proto

type Context struct {
	Data map[string]interface{}
}

func NewContext() (ctx *Context) {
	return &Context{
		Data: make(map[string]interface{}),
	}
}
