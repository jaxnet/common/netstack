package ec

import "errors"

var (
	//
	// Transport-related errors
	//
	ErrTransportFailed = errors.New("transport failed")

	//
	// Protocol-related errors.
	//
	ErrProtocolError   = errors.New("protocol error")
	ErrProtocolVersion = errors.New("protocol version is invalid")

	//
	// Messages related error.
	//
	ErrInvalidMessageType = errors.New("invalid message type")
	ErrInvalidMessageData = errors.New("invalid message data")

	//
	// Other errors.
	//
	ErrInternalStateError     = errors.New("internal state error")
	ErrInvalidStatesSet       = errors.New("invalid states set")
	ErrInvalidInteractionsSet = errors.New("invalid interactions set")
)
