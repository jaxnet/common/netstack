package protocols

import (
	"gitlab.com/jaxnet/common/netstack/stack/messages/common"
)

var (
	FinalState = []*State{nil}
)

type State struct {
	Handler          StateHandler
	IncomingMessages []common.Message
	OutgoingMessages []common.Message
	Next             []*State
}

type StateHandler func(ctx *Context, msg common.Message) (nextState *State, outgoingMessage common.Message, err error)
