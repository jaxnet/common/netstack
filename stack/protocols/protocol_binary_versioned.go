package protocols

type VersionedBinaryProtocol struct {
	BinaryProtocol
	Version uint16
}

func NewVersionedBinaryProtocol(ver uint16, interactions []Interaction) (proto *VersionedBinaryProtocol, err error) {
	lowerProtocol, err := NewBinaryProtocol(interactions)
	if err != nil {
		return
	}

	proto = &VersionedBinaryProtocol{
		Version:        ver,
		BinaryProtocol: *lowerProtocol,
	}

	return
}

func (proto *VersionedBinaryProtocol) EstablishLocalCopy() VersionedBinaryProtocol {
	c, _ := NewVersionedBinaryProtocol(proto.Version, proto.Interactions)
	return *c
}
