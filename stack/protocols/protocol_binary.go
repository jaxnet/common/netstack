package protocols

import (
	"fmt"
	"gitlab.com/jaxnet/common/netstack/stack/ec"
)

type BinaryProtocol struct {
	Interactions []Interaction

	stateIndex int
	context    interface{}
}

func NewBinaryProtocol(interactions []Interaction) (proto *BinaryProtocol, err error) {
	if len(interactions) == 0 {
		err = fmt.Errorf("interactions set can't be empty: %w", ec.ErrInvalidInteractionsSet)
		return
	}

	proto = &BinaryProtocol{
		Interactions: interactions,
		stateIndex:   0,
	}

	return
}

func (proto *BinaryProtocol) SetContext(ctx interface{}) {
	proto.context = ctx
}

func (proto *BinaryProtocol) Context() (ctx interface{}) {
	return proto.context
}

func (proto *BinaryProtocol) SetClientStateHandlers(handlers []StateHandler) (err error) {
	return proto.setHandlers(handlers, true)
}

func (proto *BinaryProtocol) SetServerStateHandlers(handlers []StateHandler) error {
	return proto.setHandlers(handlers, false)
}

func (proto *BinaryProtocol) setHandlers(handlers []StateHandler, client bool) (err error) {
	if len(proto.Interactions) != len(handlers) {
		msg := "handlers set must contain implementation for each interaction: %w"
		err = fmt.Errorf(msg, ec.ErrInvalidInteractionsSet)
		return
	}

	var state *State
	for i, handler := range handlers {

		if client {
			state = proto.Interactions[i].Client
		} else {
			state = proto.Interactions[i].Server
		}

		if state == nil {
			msg := "non initialized interaction state occurred (can't initialize handler for non-initialized state): %w"
			err = fmt.Errorf(msg, ec.ErrInvalidInteractionsSet)
			return
		}

		state.Handler = handler
	}

	return
}
