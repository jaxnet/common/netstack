package protocols

type Interaction struct {
	Client *State
	Server *State
}
