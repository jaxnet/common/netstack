package protocols

type Protocol interface {
	SetClientStateHandlers(handlers []StateHandler) error
	SetServerStateHandlers(handlers []StateHandler) error

	FirstClientState() *State
	FirstServerState() *State
}
