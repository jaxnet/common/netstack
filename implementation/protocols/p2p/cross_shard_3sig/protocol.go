package cross_shard_3sig

import (
	"gitlab.com/jaxnet/common/netstack/implementation/messages"
	"gitlab.com/jaxnet/common/netstack/implementation/protocols"
	"gitlab.com/jaxnet/common/netstack/stack/proto"
)

var (
	SenderProposalsDiscovery = &proto.State{
		IncomingMessages: nil,
		OutgoingMessages: nil,
	}

	SenderSetTxContext = &proto.State{
		IncomingMessages: nil,
		OutgoingMessages: []protocols.M{&messages.InitCrossShard3SigPayment{}},
	}

	SenderCheckTxContextResponse = &proto.State{
		IncomingMessages: []protocols.M{&messages.CrossShardInit3SigPaymentResponse{}},
	}

	SenderEADCrossShardTxPreparing = &proto.State{
		IncomingMessages: nil,
		OutgoingMessages: nil,
	}

	SenderShareCrossShardTx = &proto.State{
		IncomingMessages: nil,
		OutgoingMessages: []protocols.M{&messages.CrossShard3SigTx{}},
	}

	SenderCheckCrossShardTxResponse = &proto.State{
		IncomingMessages: []protocols.M{&messages.CrossShard3SigTxResponse{}},
		OutgoingMessages: nil,
	}
)

var (
	ReceiverSetTxContext = &proto.State{
		IncomingMessages: []protocols.M{&messages.InitCrossShard3SigPayment{}},
		OutgoingMessages: []protocols.M{&messages.CrossShardInit3SigPaymentResponse{}},
	}

	ReceiverAcceptCrossShardTx = &proto.State{
		IncomingMessages: []protocols.M{&messages.CrossShard3SigTx{}},
		OutgoingMessages: []protocols.M{&messages.CrossShard3SigTxResponse{}},
	}
)

func SenderReceiverCrossShardPayment() (protocol *proto.VersionedBinaryProtocol, err error) {

	SenderProposalsDiscovery.NextState = []protocols.S{SenderSetTxContext}
	SenderSetTxContext.NextState = []protocols.S{SenderCheckTxContextResponse}
	SenderCheckTxContextResponse.NextState = []protocols.S{SenderEADCrossShardTxPreparing}
	SenderEADCrossShardTxPreparing.NextState = []protocols.S{SenderShareCrossShardTx}
	SenderShareCrossShardTx.NextState = []protocols.S{SenderCheckCrossShardTxResponse}
	SenderCheckCrossShardTxResponse.NextState = []protocols.S{
		proto.FinalState,        // In case if response is OK.
		SenderShareCrossShardTx, // In case if TX must be sent once again.
	}

	ReceiverSetTxContext.NextState = []protocols.S{
		ReceiverAcceptCrossShardTx,
		proto.FinalState, // In case of error.
	}

	ReceiverAcceptCrossShardTx.NextState = []protocols.S{
		proto.FinalState, // In case of any error and success.
	}

	protocol, err = proto.NewVersionedBinaryProtocol(0,
		[]protocols.S{
			SenderProposalsDiscovery,
			SenderSetTxContext,
			SenderCheckTxContextResponse,
			SenderEADCrossShardTxPreparing,
			SenderShareCrossShardTx,
			SenderCheckCrossShardTxResponse,
		},
		[]protocols.S{
			ReceiverSetTxContext,
			ReceiverAcceptCrossShardTx,
		})
	return
}
