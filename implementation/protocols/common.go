package protocols

import (
	"gitlab.com/jaxnet/common/netstack/stack/messages/common"
	"gitlab.com/jaxnet/common/netstack/stack/proto"
)

type M = common.Message
type S = *proto.State
