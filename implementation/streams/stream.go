package streams

import (
	"encoding/binary"
	"fmt"
	marshalling "gitlab.com/jaxnet/common/marshaller"
	"gitlab.com/jaxnet/common/netstack/implementation/messages"
	"gitlab.com/jaxnet/common/netstack/stack/ec"
	"gitlab.com/jaxnet/common/netstack/stack/messages/common"
	"gitlab.com/jaxnet/common/netstack/stack/proto"
	"net"
)

type MessagesStream struct {
	conn net.Conn
}

func NewMessagesStream(conn net.Conn) (stream *MessagesStream) {
	stream = &MessagesStream{
		conn: conn,
	}
	return
}

func (stream *MessagesStream) SendForState(state *proto.State, msg common.Message) (err error) {
	if state.ExpectsToSendMessage(msg) == false {
		msg := "unexpected message occurred, that is not expected to be sent by current state: %w"
		err = fmt.Errorf(msg, ec.ErrProtocolError)
		return
	}

	messageTypeID, err := messages.MessageTypeID(msg)
	if err != nil {
		err = fmt.Errorf(
			"outgoing message can't be recognized by the type, "+
				"no way to set message's header, so it would not be parsed correctly on the receiver's side, "+
				"please, take a look into messages.MessageTypeID method: %w", err)
		return
	}

	encoder := marshalling.NewEncoder()
	err = encoder.PutUint16(messageTypeID)
	if err != nil {
		return
	}

	err = encoder.MarshallVariadicDataWith2BytesHeader(msg)
	if err != nil {
		return
	}

	data := encoder.CollectDataAndReleaseBuffers()

	// todo: extend this logic to re-send the rest of message
	// 		 in case if data buffer has not been fully sent.
	//		 ( write() returns int argument for checking this case)
	_, err = stream.conn.Write(data)
	if err != nil {
		return
	}

	return
}

func (stream *MessagesStream) ReadNextForState(state *proto.State) (msg common.Message, err error) {

	var (
		binaryMessageTypeID = []byte{0, 0}
		binaryMessageSize   = []byte{0, 0}
	)

	err = stream.readFixedAmountOfData(binaryMessageTypeID)
	if err != nil {
		return
	}
	messageTypeID := binary.BigEndian.Uint16(binaryMessageTypeID)

	err = stream.readFixedAmountOfData(binaryMessageSize)
	if err != nil {
		return
	}
	messageSize := binary.BigEndian.Uint16(binaryMessageSize)

	if messageSize == 0 {
		record := "malformed message: message size specified as 0: %w"
		err = fmt.Errorf(record, ec.ErrProtocolError)
		return
	}

	if messageSize > 1024*8 {
		// For security reasons, messages larger than 8KB are not allowed.
		record := "malformed message: specified message size is going to be bigger than 8KB: %w"
		err = fmt.Errorf(record, ec.ErrProtocolError)
		err = ec.ErrProtocolError
		return
	}

	msg, err = messages.ConstructMessageByTypeID(messageTypeID)
	if err != nil {
		return
	}

	if state.ExpectsToReceiveMessage(msg) == false {
		record := "unexpected message occurred, that is not expected by current state: %w"
		err = fmt.Errorf(record, ec.ErrProtocolError)
		return
	}

	binaryMessageBody := make([]byte, messageSize)
	err = stream.readFixedAmountOfData(binaryMessageBody)
	if err != nil {
		return
	}

	decoder := marshalling.NewDecoder(binaryMessageBody)
	err = decoder.UnmarshalDataSegment(int(messageSize), msg)
	return
}

func (stream *MessagesStream) readFixedAmountOfData(dst []byte) (err error) {
	bytesRead, err := stream.conn.Read(dst)
	if err != nil {
		return
	}

	if bytesRead != len(dst) {
		err = ec.ErrProtocolError
		return
	}

	return
}
