package messages

import (
	"gitlab.com/jaxnet/common/netstack/stack/ec"
	"gitlab.com/jaxnet/common/netstack/stack/messages/common"
)

const (
	init3SigCrossShardPayment         = 1000
	init3SigCrossShardPaymentResponse = 1001
	shareCrossShardTX                 = 1010
	shareCrossShardTXResponse         = 1011
)

func MessageTypeID(msg common.Message) (id uint16, err error) {
	switch msg.(type) {
	case *InitCrossShard3SigPayment:
		id = init3SigCrossShardPayment

	case *CrossShardInit3SigPaymentResponse:
		id = init3SigCrossShardPaymentResponse

	case *CrossShard3SigTx:
		id = shareCrossShardTX

	case *CrossShard3SigTxResponse:
		id = shareCrossShardTXResponse

		//
		// Other messages types goes here.
		//

	default:
		err = ec.ErrInvalidMessageType
	}

	return
}

func ConstructMessageByTypeID(typeID uint16) (msg common.Message, err error) {
	switch typeID {
	case init3SigCrossShardPayment:
		msg = &InitCrossShard3SigPayment{}

	case init3SigCrossShardPaymentResponse:
		msg = &CrossShardInit3SigPaymentResponse{}

	case shareCrossShardTX:
		msg = &CrossShard3SigTx{}

	case shareCrossShardTXResponse:
		msg = &CrossShard3SigTxResponse{}

		//
		// Other messages types goes here.
		//

	default:
		err = ec.ErrInvalidMessageType
	}

	return
}
