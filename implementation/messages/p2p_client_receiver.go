package messages

import (
	"github.com/google/uuid"
	"gitlab.com/jaxnet/common/marshaller"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txmodels"
)

const (
	CodeOK = 0

	//
	// Common error codes go here
	//
	CodeErrUnexpectedMessage         = 30
	CodeErrUnexpectedTransactionUUID = 31

	CodeErrServerFault = 255
)

type InitCrossShard3SigPayment struct {
	SourceShardID      uint32
	DestinationShardID uint32
	SenderPubKey       *btcec.PublicKey
	EADPubKey          *btcec.PublicKey
	Amount             btcutil.Amount
}

func (msg *InitCrossShard3SigPayment) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.PutUint32(msg.SourceShardID)
	if err != nil {
		return
	}

	err = encoder.PutUint32(msg.DestinationShardID)
	if err != nil {
		return
	}

	senderKeyBinary := msg.SenderPubKey.SerializeCompressed()
	err = encoder.PutVariadicDataWith2BytesHeader(senderKeyBinary)
	if err != nil {
		return
	}

	eadKeyBinary := msg.EADPubKey.SerializeCompressed()
	err = encoder.PutVariadicDataWith2BytesHeader(eadKeyBinary)
	if err != nil {
		return
	}

	err = encoder.PutInt64(int64(msg.Amount))
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (msg *InitCrossShard3SigPayment) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	msg.SourceShardID, err = decoder.GetUint32()
	if err != nil {
		return
	}

	msg.DestinationShardID, err = decoder.GetUint32()
	if err != nil {
		return
	}

	senderPubKeyBinary, err := decoder.GetDataSegmentWith2BytesHeader()
	if err != nil {
		return
	}

	msg.SenderPubKey, err = btcec.ParsePubKey(senderPubKeyBinary, btcec.S256())
	if err != nil {
		return
	}

	eadPubKeyBinary, err := decoder.GetDataSegmentWith2BytesHeader()
	if err != nil {
		return
	}

	msg.EADPubKey, err = btcec.ParsePubKey(eadPubKeyBinary, btcec.S256())
	if err != nil {
		return
	}

	amount, err := decoder.GetInt64()
	if err != nil {
		return
	}

	msg.Amount = btcutil.Amount(amount)
	return
}

type CrossShardInit3SigPaymentResponse struct {
	TxUUID uuid.UUID
	Code   uint8
}

func (msg *CrossShardInit3SigPaymentResponse) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.MarshallFixedSizeDataSegment(msg.TxUUID)
	if err != nil {
		return
	}

	err = encoder.PutUint8(msg.Code)
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (msg *CrossShardInit3SigPaymentResponse) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	err = decoder.UnmarshalDataSegment(16, &msg.TxUUID)
	if err != nil {
		return
	}

	msg.Code, err = decoder.GetUint8()
	return
}

type CrossShard3SigTx struct {
	TxUUID uuid.UUID
	Tx     *txmodels.SwapTransaction
}

func (msg *CrossShard3SigTx) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.MarshallFixedSizeDataSegment(msg.TxUUID)
	if err != nil {
		return
	}

	err = encoder.MarshallVariadicDataWith2BytesHeader(msg.Tx)
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (msg *CrossShard3SigTx) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	err = decoder.UnmarshalDataSegment(16, &msg.TxUUID)
	if err != nil {
		return
	}

	msg.Tx = &txmodels.SwapTransaction{}
	err = decoder.UnmarshalDataSegmentWith2BytesHeader(msg.Tx)
	return
}

type CrossShard3SigTxResponse struct {
	Code     uint8
	SignedTx *txmodels.SwapTransaction
}

func (msg *CrossShard3SigTxResponse) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.PutUint8(msg.Code)
	if err != nil {
		return
	}

	if msg.Code == CodeOK && msg.SignedTx != nil {
		err = encoder.MarshallVariadicDataWith2BytesHeader(msg.SignedTx)
		if err != nil {
			return
		}
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (msg *CrossShard3SigTxResponse) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)
	msg.Code, err = decoder.GetUint8()
	if err != nil {
		return
	}

	if msg.Code == CodeOK {
		msg.SignedTx = &txmodels.SwapTransaction{}
		err = decoder.UnmarshalDataSegmentWith2BytesHeader(msg.SignedTx)
		if err != nil {
			return
		}
	}

	return
}
